module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ['google', 'plugin:prettier/recommended'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint'],
  rules: {
    'prettier/prettier': ['error', {}, {usePrettierrc: true}],
    indent: ['error', 2],
    'require-jsdoc': 'off',
    'object-curly-spacing': 'off',
    'quote-props': 'off',
  },
};
